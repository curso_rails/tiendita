Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users
  resources :categories

  root 'articles#index'
  resources :articles
  namespace :api do
    resources :articles do

      member do
        get "categories" #/api/articles/:id/categories
      end

      collection do
        get 'categories2' #/api/articles/categories2
      end

    end
  end

end
