class Article < ActiveRecord::Base

	belongs_to :user
	has_and_belongs_to_many :categories

	has_attached_file :imagen, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  	validates_attachment_content_type :imagen, :content_type => /\Aimage\/.*\Z/

	validates :name,  presence: true, uniqueness: true
	validates :description,  presence: true
	validates :price,  presence: true
	#validates_presence_of :name,  message:"No debe de estar vacio"
	scope :ordenados, -> { order("created_at DESC")}

end
