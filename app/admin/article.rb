ActiveAdmin.register Article do
	permit_params :name, :description, :price, :imagen


	index do
		selectable_column
		id_column
		column :name
		column :price
		column :user
		column(:imagen) {|article| image_tag article.imagen.url( :thumb) }

		column :categories
		actions
	end


	form do|f|
		f.inputs "Articulo" do
			f.input :name
			f.input :description
			f.input :price
			f.input :imagen
		end
		f.actions

	end
end
