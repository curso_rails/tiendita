class ArticlesController < ApplicationController
	before_action :authenticate_user!, except: [:index, :show]
	before_action :categories, only: [:create, :update]

	#layout 'base', except: [:show]
	#layout 'base', only: [:show]

	#get /articles
	def index
		@articles = Article.all.ordenados
	end

	#get /articles/new
	def new
		@article = Article.new
	end

	#get /articles/:id
	def show
		@article = Article.find(params[:id])
	end

	#put /articles/:id
	#patch /articles/:id
	def update
		@article = Article.find(params[:id])
		if @article.update_attributes(article_params )
			@article.categories = @categories
			redirect_to @article
		else
			render :edit
		end
	end


	#post /articles
	def create
		@article = current_user.articles.new(article_params)
		#@article = Article.new(article_params)
		if @article.save
			@article.categories = @categories
			redirect_to @article
		else
			render :new
		end
	end

	#get /articles/:id/edit
	def edit
		@article = Article.find(params[:id])
	end

	#delete /articles/:id
	def destroy
	end

	private
	def article_params
		params.require(:article).permit(:name, :description, :price, :imagen)
	end

	def categories
		unless params[:article][:category_ids].nil?
			 @categories = params[:article][:category_ids].map{|k, v| Category.find(k)}
		else
			@categories = []
		end
	end

end
